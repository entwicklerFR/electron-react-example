import React, { Component, Fragment } from "react";
//import { remote } from "electron";

const isElectron = () => {
  // https://www.npmjs.com/package/is-electron

  // Renderer process 
  if (typeof window !== 'undefined' && typeof window.process === 'object' && window.process.type === 'renderer') {
    return true;
  }

  // Main process 
  if (typeof process !== 'undefined' && typeof process.versions === 'object' && !!process.versions.electron) {
    return true;
  }

  // Detect the user agent when the `nodeIntegration` option is set to true 
  if (typeof navigator === 'object' && typeof navigator.userAgent === 'string' && navigator.userAgent.indexOf('Electron') >= 0) {
    return true;
  }

  return false;
}

class App extends Component {

  render() {

    let node;
    let navigator;
    let electron;
    let platform;
    let message = "in Browser"

    if (isElectron()) {
      message = "in Electron"
      node = window.remote.process.versions.node;
      navigator = window.remote.process.versions.chrome;
      electron = window.remote.process.versions.electron;
      platform = window.remote.process.platform;
    } else {
      node = "not available";
      navigator = window.navigator.appName + "(" + window.navigator.appVersion + ")";
      electron = "not available";
      platform = window.navigator.platform;
    }

    return (
      <Fragment>
        <h1>Hello World!</h1>
        <h2>I'm a react web page displayed <span style={{ color: 'red' }}>{message}</span>.</h2>
        We are using
        <br /><br />Node {node}
        <br />Navigator {navigator}
        <br />Electron {electron}
        <br /><br />on platform {platform}
      </Fragment>
    );
  }
}

export default App;