var path = require('path');

module.exports = {
  mode: 'none',

  entry: [
    path.resolve(__dirname, "./react/src/main.js")
  ],

  output: {
    path: path.resolve(__dirname, "./react/build"),
    filename: "bundle.js"    
  },

  //target: 'electron-renderer',

  module: {

    rules: [
      /* javascript */
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react']
          }
        }
      },

      /* jsx */
      {
        test: /\.jsx$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react']
          }
        }
      }
    ]

  },

  /* our './myComponent' requires or imports are equivalent to 'myComponent.jsx' */
  resolve: {
    extensions: ['*', '.js', '.jsx']
  }
}
